import React, {Component} from "react"

 class EditFilm extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.name !== this.props.name
    }

     render() {
        return(
            <div>
                <input type="text" value={this.props.name} onChange={ event => this.props.editHandler(event ,this.props.index)}/>
                <button onClick={this.props.removeFilmHandler} className="del-btn btn">X</button>
            </div>
        )
    }
}
export default EditFilm;
