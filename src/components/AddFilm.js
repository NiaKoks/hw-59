import React from "react"
 const AddFilm = props =>{
     console.log(props);
     return(
        <div className="add-film">
            <input value={props.filmName} onChange={(event) => props.filmNameHandler(event)}className="add-inp" placeholder="Name of the premier" type="text"/>
            <button onClick={props.addFilmHandler}  className="add-btn btn">Add</button>
        </div>
    )
 };
export default AddFilm;