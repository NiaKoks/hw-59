import React, { Component } from 'react';
import './App.css';
import AddFilm from "./components/AddFilm";
import EditFilm from "./components/EditFilm";
class App extends Component {
  state = {
    films: [],
    filmName: '',
    filmData: ''
  };

  filmNameChangeHandler = (event) =>{
    this.setState({filmName:event.target.value})
  };
  editHandler = (event, index) => {
    const films = this.state.films;
    films[index].filmName = event.target.value;
    this.setState({ films })
  };
  addFilm = () =>{
    let newFilm ={filmName: this.state.filmName, filmData: this.state.filmData};
    let films = [...this.state.films, newFilm];
    this.setState({films})
  };
  delFilm = (name) =>{
    let films = [...this.state.films];
    const id = films.findIndex(filmName => films.filmName === name);
    films.splice(id,1);
    this.setState({films: films})
  };
  render() {
    return (
      <div className="App">
        <h2>Films That I'm Waiting To</h2>
        <AddFilm filmName = {this.state.filmName}
                 filmNameHandler={this.filmNameChangeHandler}
                 addFilmHandler={this.addFilm}>

        </AddFilm>
        <p>To Watch List:</p>
        {this.state.films.map((film, index) => {
          return (
              <EditFilm index={index} name={film.filmName}  removeFilmHandler ={this.delFilm} editHandler={this.editHandler} key = {index}/>
          )
        })}
      </div>
    );
  }
}

export default App;
